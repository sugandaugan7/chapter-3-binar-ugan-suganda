const express = require('express');
const app = express();
const port = 2500;

// middleware
const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
}
app.use(logger);
app.use(express.json());
app.use(express.static('public'));
app.use(express.urlencoded({ extended: false}));

app.set('view engine', 'ejs');

// nyalakan web server
app.listen(port, () => {
    console.log(`Server at http://localhost:${port}`)
});